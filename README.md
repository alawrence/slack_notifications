# Slack Notifications

Drupal 8 module to send comment notifications to a Slack channel using Slack Incoming webhooks.

This module was created as a how-to article on my blog, [Dadveloper.rocks](https://www.dadveloper.rocks/how-drupal-8-slack-notifications-module). Check out the article and feel free either create this module yourself or clone it, extend it and create a pull request with your improvements!

