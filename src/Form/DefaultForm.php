<?php

namespace Drupal\slack_notifications\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DefaultForm.
 */
class DefaultForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'slack_notifications.default',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('slack_notifications.default');
    $form['slack_webhook_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slack Webhook URL'),
      '#description' => $this->t('Enter the URL of a Slack Incoming Webhook.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('slack_webhook_url'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('slack_notifications.default')
      ->set('slack_webhook_url', $form_state->getValue('slack_webhook_url'))
      ->save();
  }

}
