<?php

namespace Drupal\slack_notifications\Library;

/**
 * Class SlackMessage
 * @package Drupal\slack_notifications\Library
 */
class SlackMessage {

    /**
     * push
     *
     * @param $message
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function push($message) {
        $config = \Drupal::config('slack_notifications.default');
        $client = \Drupal::httpClient();
        $url = $config->get('slack_webhook_url');
        $body = new \stdClass();
        $body->text = $message;
        try {
            $res = $client->request('POST', $url, [
                \GuzzleHttp\RequestOptions::JSON => $body,
            ]);
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            \Drupal::logger('slack_notifications')->error($e->getMessage());
            return false;
        }
        return true;
    }
}